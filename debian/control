Source: purple
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3,
               markdown
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/med-team/purple
Vcs-Git: https://salsa.debian.org/med-team/purple.git
Homepage: https://gitlab.com/rki_bioinformatics/purple

Package: purple
Architecture: any
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-biopython,
         python3-tqdm,
         python3-yaml
Description: Picking Unique Relevant Peptides for viraL Experiments
 Emerging virus diseases present a global threat to public health. To
 detect viral pathogens in time-critical scenarios, accurate and fast
 diagnostic assays are required. Such assays can now be established using
 mass spectrometry-based targeted proteomics, by which viral proteins can
 be rapidly detected from complex samples down to the strain level with
 high sensitivity and reproducibility. Developing such targeted assays
 involves tedious steps of peptide candidate selection, peptide
 synthesis, and assay optimization. Peptide selection requires extensive
 preprocessing by comparing candidate peptides against a large search
 space of background proteins. Purple (Picking Unique Relevant Peptides
 for viraL Experiments) is a software tool for selecting target-specific
 peptide candidates directly from given proteome sequence data.
 .
 Purple comes with an intuitive graphical user interface, various
 parameter options and a threshold-based filtering strategy for
 homologous sequences.
 .
 Purple enables peptide candidate selection across various taxonomic
 levels and filtering against backgrounds of varying complexity. Its
 functionality is demonstrated using data from different virus species
 and strains. Purple enables building taxon-specific targeted assays
 and paves the way to time-efficient and robust viral diagnostics using
 targeted proteomics.
