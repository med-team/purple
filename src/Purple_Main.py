import Purple_methods as purple
import os.path
import re
from timeit import default_timer as timer
import argparse
from shutil import rmtree


if __name__ == '__main__':
    print("    ######  #     # ######  ######  #       #######")
    print("    #     # #     # #     # #     # #       #      ")
    print("    #     # #     # #     # #     # #       #      ")
    print("    ######  #     # ######  ######  #       #####  ")
    print("    #       #     # #   #   #       #       #      ")
    print("    #       #     # #    #  #       #       #      ")
    print("    #        #####  #     # #       ####### #######")
    print("Picking Unique Relevant Peptides for viraL Experiments")
    print("                    Version: 0.4.0\n")
    print("\n#############################\n# Start reading config file #\n#############################\n")
    parser = argparse.ArgumentParser(description='Purple arguments', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    group0 = parser.add_mutually_exclusive_group()
    group0.add_argument('-c', '--config', metavar='FILE', type=str, help='an config file')
    group = parser.add_argument_group('important for purple')
    group3 = parser.add_argument_group('can easily stay default')
    group.add_argument('--target', metavar='STRING', default='Hepatitis B virus', type=str, help='target; please use a dot . between different words for the others as well')
    group.add_argument('--threshold', metavar='INTEGER', default=70, type=int, help='threshold percentage')
    group.add_argument('--update', metavar='BOOL', default=False, type=bool, help='update the database')
    group.add_argument('--pathDB', metavar='STRING', default='../res/DB/', type=str, help='path to the database')
    group.add_argument('--pathO', metavar='STRING', default='../output/', type=str, help='path to the results')
    group.add_argument('--targetFile', metavar='STRING', default='', type=str, help='File name of target fasta')
    group3.add_argument('--minL', metavar='INTEGER', default=5, type=int, help='minimal length of the peptides')
    group3.add_argument('--maxL', metavar='INTEGER', default=25, type=int, help='maximal length of the peptides')
    group3.add_argument('--question', metavar='BOOL', default=True, type=bool, help='ask if the target is right')
    group3.add_argument('--printP', metavar='BOOL', default=False, type=bool, help='print the unique peptides in the console')
    group3.add_argument('--prolineD', metavar='BOOL', default=False, type=bool, help='Enable proline digestion rule')
    group3.add_argument('--leucineD', metavar='BOOL', default=False, type=bool, help='Enable distinction between leucine and isoleucine')
    group3.add_argument('--comment', metavar='STRING', default='no comment', type=str, help='comment for purples log-book')

    args = parser.parse_args()
    
    if args.config is not None:
        config_file = args.config
        if '.yml' in config_file or '.yaml' in config_file:
            config = purple.load_config_yml(config_file)
        else:
            exit('The data format of the configuration file is not supported! Please use a .json-, .yml- or .yaml-file!')
        
        print("Config file: ",config_file)
        
        print("\n- Purple: ")
        target = config['purple']['target']
        print("\tTarget: ",target)
        threshold = int(config['purple']['threshold'])
        print("\tThreshold: ",threshold)
        path_DB = str(config['purple']['path_DB'])
        print("\tPath to databases: ",path_DB)
        update_DB = bool(config['purple']['update_DB'])
        print("\tUpdate database: ",update_DB)
        targetFile = config['purple']['targetFile']
        print("\ttargetFile: ",targetFile)

        min_len_peptides = int(config['purple']['min_len_peptides'])
        print("\tMinimum length of peptide: ",min_len_peptides)
        max_len_peptides = int(config['purple']['max_len_peptides'])
        print("\tMaximum length of peptide: ",max_len_peptides)
        removeFragments = bool(config['purple']['removeFragments'])
        print("\tExclude fragments from fasta files: ",removeFragments)

        path_output = str(config['purple']['path_output'])
        print("\tPath to output: ",path_output)
        i_am_not_sure_about_target = bool(config['purple']['i_am_not_sure_about_target'])
        print("\tAre you sure about the target: ",i_am_not_sure_about_target)
        leucine_distincion = bool(config['purple']['leucine_distincion'])
        print("\tLeucine and Isoleucine distinction: ",leucine_distincion)
        proline_digestion = bool(config['purple']['proline_digestion'])
        print("\tProline digestion rule: ",proline_digestion)
        print_peptides = bool(config['purple']['print_peptides'])
        print("\tPrint peptides: ",print_peptides)
        comment = str(config['purple']['comment'])
        print("\tComment: ",comment)

    
    else:

        target = args.target.lower()
        target = re.sub(r'[^a-zA-z0-9()/-]', ' ', target)
        threshold = args.threshold
        path_DB = args.pathDB
        update_DB = args.update
        targetFile = ""
        
        min_len_peptides = args.minL
        max_len_peptides = args.maxL

        path_output = args.pathO
        i_am_not_sure_about_target =args.question
        print_peptides = args.printP
        comment = args.comment
        comment = re.sub('[#,.-_:;*+]', ' ', comment)
    
        
    # get path of this script
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)
    print("\nWorking directory: ",dname)
    
    # Clean database from targets
    if(not targetFile is None):
        target = purple.cleanAllDBs(path_DB,targetFile)
        path_DB = path_DB+"/clean/"

    # if requested, creation of new BG
    purple.prepare_new_BG_file(update_DB, path_DB, min_len_peptides, max_len_peptides, removeFragments,leucine_distincion,proline_digestion)

    # load DBs
    BG = purple.loadBG(path_DB)
    species_list = purple.loadOS(path_DB)

    parameter = [target, threshold, update_DB, path_DB, min_len_peptides, max_len_peptides]
    purple.print_parameter('console', parameter)
    print(len(BG))

    # Check search-string against species list
    targets, Nr_target_peptides = purple.do_you_mean_this_target_GUI(target, species_list, i_am_not_sure_about_target)

    # Add output folder for experiment
    path_output = purple.createOutputDir(path_output,targets) 

    timeStart = timer()

    # Extract target from BG / Exact Matching
    uniques, shared, BG = purple.extract_target_from_BG(BG, targets, path_output)

    # Write exact unique peptides to tsv file
    purple.writeExactPeptidesToTSV(path_output,uniques)

    # Runtime prediction
    print('\nNumber of found peptides without matching:', len(uniques))

    # Inexact matching
    unique_peptides_scores = purple.matching2(uniques, BG, threshold)

    # Add output folder for experiment
    purple.writeInexactPeptidesToTSV(path_output,unique_peptides_scores,uniques,threshold) 

    # Print results
    freq_scores = purple.print_result(unique_peptides_scores, print_peptides,threshold)
    purple.create_output_file(uniques, unique_peptides_scores, target, path_output,threshold)
    purple.writeSharedPeptidesToTSV(path_output,shared,uniques,unique_peptides_scores,threshold)

    # Write record in log file
    timeEnd = timer()
    runtime = round(timeEnd - timeStart, 2)
    print ('Runtime: ', runtime, 'sec, or', round(runtime / 60, 1), 'minutes.')
    parameter.extend([targets, runtime, comment, len(uniques), freq_scores, Nr_target_peptides])
    purple.print_parameter('file', parameter, path_output)

    # Delete clean directory
    if(update_DB):
        rmtree(path_DB)
    
"""
Created on Sep 08 2017

@author: LechnerJ
@author: HiortP
@author: HartkopfF
If there are any questions feel free to contact me:
    johanna.lechner@t-online.de
    p.hiort[at]web.de
"""