###########
# Imports #
###########

import tkinter as tk
import sys
import threading
from tkinter import filedialog, IntVar
import yaml
from tkinter import ttk
import time
import Purple_methods as purple 

###########
# Classes #
###########

class CreateToolTip(object):
    '''
    create a tool tip for a given widget
    '''
    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.close)
    def enter(self, *unused):
        location = self.widget.bbox("insert")
        x = location[0]
        y = location[1]
        x += self.widget.winfo_rootx() + 0
        y += self.widget.winfo_rooty() + 50
        # creates a top level window
        self.tw = tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(self.tw, text=self.text, justify='left',
                       background='lightyellow', relief='solid', borderwidth=1,
                       font=("times", "8", "normal"))
        label.pack(ipadx=1)
    def close(self, *unused):
        if self.tw:
            self.tw.destroy()


class Std_redirector(object):
    def __init__(self,widget):
        self.widget = widget

    def write(self,string):
        self.widget.insert(tk.END,string)
        self.widget.see(tk.END)

###########
# Methods #
###########

def startPurple():
    global thread1
    global thread2
    thread1 = threading.Thread(target = purple.main, args = ["config.yml"])
    thread1.daemon = True
    thread1.start()
    thread2 = threading.Thread(target=showAnimation)
    thread2.daemon=True
    thread2.start()
    root.after(0, updateFrames, 0)

def showAnimation():
    global thread1
    loadingAni.grid(row=0,column=boxCol, columnspan=1, rowspan=1)
    while(thread1.isAlive()):
        time.sleep(1)
    loadingAni.grid_forget()
    del(thread1)
    

def stopPurple():
    print("Please wait to end.")
        
def exePurpleMain():
    exec(open('Run_Purple.py').read())

def selectTarget():
    global setTargetButtonText
    global cfg
    path = filedialog.askopenfilename(initialdir = "/",title = "Select target file")
    print("path", path)

    splitPath = path.split(sep='/')
    cfg['purple']['targetFile'] = splitPath[len(splitPath)-1]
    setTargetButtonText.set(splitPath[len(splitPath)-1])
    if path == "":
        setTargetButtonText.set("Browse...")
    with open("config.yml", 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
        
def selectDBs():
    global dbPath
    global cfg
    path = filedialog.askdirectory(initialdir = "/",title = "Select database directory")
    cfg['purple']['path_DB'] = path
    dbPath.set(path)
    with open("config.yml", 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)

def selectOutput():
    global outputPath
    global cfg
    path = filedialog.askdirectory(initialdir = "/",title = "Select output directory")
    cfg['purple']['path_output'] = path
    outputPath.set(path)
    with open("config.yml", 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
        
def setThreshold(self):
    global threshold
    global cfg
    cfg['purple']['threshold'] = threshold.get()
    with open("config.yml", 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
        
def setMinLen():
    global minLen
    global cfg
    cfg['purple']['min_len_peptides'] = int(minLen.get())
    with open("config.yml", 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True

def setMaxLen():
    global maxLen
    global cfg
    cfg['purple']['max_len_peptides'] = int(maxLen.get())
    with open("config.yml", 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True


def setTargetList():
    global targetList
    global setTargetButtonText
    global cfg
    cfg['purple']['target'] = targetList.get().replace(' ', '').split(sep=",")
    setTargetButtonText.set('Select file...')
    cfg['purple']['targetFile'] = ""
    with open("config.yml", 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True

def setTitle():
    global title
    global cfg
    cfg['purple']['comment'] = title.get()
    with open("config.yml", 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True

def setDB():
    global dbPath
    global cfg
    cfg['purple']['path_DB'] = dbPath.get()
    with open("config.yml", 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True

def setOutput():
    global outputPath
    global cfg
    cfg['purple']['path_output'] = outputPath.get()
    with open("config.yml", 'w') as ymlfile:
        yaml.dump(cfg, ymlfile)
    return True

def toggle():
    '''
    use
    t_btn.config('text')[-1]
    to get the present state of the toggle button
    '''
    if t_btn.config('text')[-1] == 'Target list':
        t_btn.config(text='Target file')
        setTargetButton.grid(row=3,column=boxCol, padx=10, pady=0)
        targetListBox.grid_forget()
    else:
        t_btn.config(text='Target list')
        setTargetButton.grid_forget()
        targetListBox.grid(row=3,column=boxCol, padx=0, pady=0,sticky='we')   
        
def togglePrintPeptide():
    global cfg
    if togglePeptides.config('relief')[-1] == 'sunken':
        togglePeptides.config(relief="raised")
        togglePeptides.config(background="light salmon")
        cfg['purple']['print_peptides'] = False
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        togglePeptides.config(relief="sunken")
        togglePeptides.config(background="lightgreen")
        cfg['purple']['print_peptides'] = True
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
            
def toggleRemoveFragments():
    global cfg
    if toggleFragments.config('relief')[-1] == 'sunken':
        toggleFragments.config(relief="raised")
        toggleFragments.config(background="light salmon")
        cfg['purple']['removeFragments'] = False
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        toggleFragments.config(relief="sunken")
        toggleFragments.config(background="lightgreen")
        cfg['purple']['removeFragments'] = True
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
            
def toggleUpdateDB():
    global cfg
    if toggleUpdateDB.config('relief')[-1] == 'sunken':
        toggleUpdateDB.config(relief="raised")
        toggleUpdateDB.config(background="light salmon")
        cfg['purple']['update_DB'] = False
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        toggleUpdateDB.config(relief="sunken")
        toggleUpdateDB.config(background="lightgreen")
        cfg['purple']['update_DB'] = True
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
            
def toggleCheckTargets():
    global cfg
    if toggleCheckTargets.config('relief')[-1] == 'sunken':
        toggleCheckTargets.config(relief="raised")
        toggleCheckTargets.config(background="light salmon")
        cfg['purple']['i_am_not_sure_about_target'] = False
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        toggleCheckTargets.config(relief="sunken")
        toggleCheckTargets.config(background="lightgreen")
        cfg['purple']['i_am_not_sure_about_target'] = True
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
            
def toggleDistinction():
    global cfg
    if toggleDistinction.config('relief')[-1] == 'sunken':
        toggleDistinction.config(relief="raised")
        toggleDistinction.config(background="light salmon")
        cfg['purple']['leucine_distincion'] = False
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        toggleDistinction.config(relief="sunken")
        toggleDistinction.config(background="lightgreen")
        cfg['purple']['leucine_distincion'] = True
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
            
def toggleProline():
    global cfg
    if toggleProline.config('relief')[-1] == 'sunken':
        toggleProline.config(relief="raised")
        toggleProline.config(background="light salmon")
        cfg['purple']['proline_digestion'] = False
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)
    else:
        toggleProline.config(relief="sunken")
        toggleProline.config(background="lightgreen")
        cfg['purple']['proline_digestion'] = True
        with open("config.yml", 'w') as ymlfile:
            yaml.dump(cfg, ymlfile)

def updateFrames(ind):
    frame = frames[ind]
    if ind <=2:
        ind += 1
    else:
        ind = 0
    loadingAni.configure(image=frame)
    root.after(120, updateFrames, ind)

def hello():
    print("hello!")

###################
# Root properties #
###################
root = tk.Tk()
root.iconbitmap('icon/Purple_64_basic.ico')
root.title("Purple 0.4.1")
#root.geometry("780x380") #You want the size of the app to be 500x500
#root.resizable(0, 0) # Don't allow resizing in the x or y direction

# Column of name labels
namesCol = 7
# Column of entry boxes
boxCol = 9
# Width of toggles
toggleWidth = 20

# Top bar icon
iconPath = "icon/Purple_128.png"
logo = tk.PhotoImage(file=iconPath)
w1 = tk.Label(root, image=logo).grid(row=0,column=boxCol, columnspan=1, rowspan=1)

# Loading animation
frames = [tk.PhotoImage(file='icon/loading_128.gif',format = 'gif -index %i' %(i)) for i in range(4)]
loadingAni = tk.Label(root)

############
# Menu bar #
############
menubar = tk.Menu(root)

# create a pulldown menu, and add it to the menu bar
filemenu = tk.Menu(menubar, tearoff=0)
filemenu.add_command(label="Open", command=hello)
filemenu.add_command(label="Save", command=hello)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=root.quit)
menubar.add_cascade(label="File", menu=filemenu)

# create more pulldown menus
editmenu = tk.Menu(menubar, tearoff=0)
editmenu.add_command(label="Cut", command=hello)
editmenu.add_command(label="Copy", command=hello)
editmenu.add_command(label="Paste", command=hello)
menubar.add_cascade(label="Edit", menu=editmenu)

helpmenu = tk.Menu(menubar, tearoff=0)
helpmenu.add_command(label="About", command=hello)
menubar.add_cascade(label="Help", menu=helpmenu)

# display the menu
root.config(menu=menubar)

# Console
scrollbar = tk.Scrollbar(root,orient="vertical",)
scrollbar.grid(row=17,column=17, sticky='ns', columnspan=1, rowspan=16)
text = tk.Text(root, yscrollcommand=scrollbar.set,bg="black", fg="white")
sys.stdout = Std_redirector(text)
text.grid(row=17,column=0, columnspan=16, rowspan=16)

# config
with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)
print('Loading latest configurations...')
for section in cfg:
    for entry in cfg[section]:
        print('\t',entry,':',cfg[section][entry])


# Start and Stop
pill2kill = threading.Event()
thread1 = threading.Thread(target=exePurpleMain, args=(pill2kill, "task"))
#stop_button = ttk.Button(root,text='Cancel',command=stopPurple)
#stop_button.grid(row=14,column=boxCol, padx=50, pady=10, sticky='e')
start_button = ttk.Button(root,text='Start Purple',command=startPurple)
start_button.grid(row=14,column=boxCol, padx=0, pady=10, sticky='we')

##########
# Config #
##########

# Path to DBs
dbPath = tk.StringVar()
dbPath.set(cfg['purple']['path_DB'])
browseText = tk.StringVar()
browseText.set("Browse...")
tk.Label(root, text="Path to DBs:").grid(row=1,column=namesCol, padx=0, pady=0, sticky='w')
setDBButton = ttk.Button(root,textvariable = browseText,command=selectDBs)
setDBButton.grid(row=1,column=boxCol+1, padx=10, pady=0)
setDBBox = ttk.Entry(root,width=50, textvariable = dbPath, validate ='focus',validatecommand=setDB)
setDBBox.grid(row=1,column=boxCol, padx=0, pady=0, sticky='we')   
setDB_ttp = CreateToolTip(setDBButton, "Select database folder containing target and background fasta databases for the Purple analysis.")

# Title
titleLabel = tk.Label(root, text="Title:").grid(row=2,column=namesCol, padx=0, pady=0, sticky='w')
title = tk.StringVar()
title.set(cfg['purple']['comment'])
titleBox = ttk.Entry(root,width=50, textvariable = title, validate ='focus',validatecommand=setTitle)
titleBox.grid(row=2,column=boxCol, padx=0, pady=0,sticky = 'we')   
title_ttp = CreateToolTip(titleBox, "Add title that is used for the output.")

# Toggle between target list and target file
t_btn = ttk.Button(text="Target file", width=12, command=toggle)
t_btn.grid(row=3,column=namesCol, sticky='w')

# Target file
setTargetButtonText = tk.StringVar()
setTargetButtonText.set(cfg['purple']['targetFile'])
if setTargetButtonText.get() == "":
    setTargetButtonText.set("Browse...")
setTargetButton = ttk.Button(root,textvariable = setTargetButtonText,command=selectTarget)
setTargetButton.grid(row=3,column=boxCol, padx=10, pady=5)
setTarget_ttp = CreateToolTip(setTargetButton, "Select a target database from the databases selected above. ")

# Target list
targetList = tk.StringVar()
targetListBox = ttk.Entry(root,width=30, textvariable = targetList, validate ='focus',validatecommand=setTargetList)
targetList_ttp = CreateToolTip(targetListBox, "Provide a list of targets separated by a comma. These names are used to identify the target and can be subsequences of the target species.")

# Threshold
thresholdLabel = tk.Label(root, text="Inexact matching threshold:").grid(row=4,column=namesCol, padx=0, pady=0, sticky='w')
threshold = IntVar()
threshold.set(int(cfg['purple']['threshold']))
thresholdScale = tk.Scale(root, from_=0, to=100, orient='horizontal',length=100,variable = threshold,command=setThreshold)
thresholdScale.grid(row=4,column=boxCol, padx=0, pady=0, columnspan=1,sticky='we')
Scale_ttp = CreateToolTip(thresholdScale, "Specify the alignment threshold for the inexact matching.")

# Sequence length
tk.Label(root, text="Sequence length:").grid(row=5,column=namesCol, padx=0, pady=0, sticky='w')
minLen = tk.StringVar()
minLen.set(cfg['purple']['min_len_peptides'])
maxLen = tk.StringVar()
maxLen.set(cfg['purple']['max_len_peptides'])
minLenBox = ttk.Entry(root,width=6, textvariable = minLen, validate ='focus',validatecommand=setMinLen)
minLenBox.grid(row=5,column=boxCol, padx=70, pady=0, sticky='w')
tk.Label(root, text="to").grid(row=5,column=boxCol, padx=0, pady=0)
maxLenBox = ttk.Entry(root,width=6, textvariable = maxLen, validate ='focus',validatecommand=setMaxLen)
maxLenBox.grid(row=5,column=boxCol, padx=70, pady=0, sticky='e')
minLen_ttp = CreateToolTip(minLenBox, "Specify minimum length of peptides.")
maxLen_ttp = CreateToolTip(maxLenBox, "Specify maximum length of peptides.")

# Print peptides
tk.Label(root, text="Additional options:").grid(row=6,column=namesCol, padx=0, pady=0, sticky='w')
if(cfg['purple']['print_peptides']):
    togglePeptides = tk.Button(text="Print peptides", width=toggleWidth, relief="sunken",background="lightgreen", command=togglePrintPeptide)
else:
    togglePeptides = tk.Button(text="Print peptides", width=toggleWidth, relief="raised",background="light salmon", command=togglePrintPeptide)
togglePeptides.grid(row=6,column=boxCol,sticky="w", pady=5)
printPeptides_ttp = CreateToolTip(togglePeptides, "Print peptides at the end of Purple.")

# Remove fragments
if(cfg['purple']['removeFragments']):
    toggleFragments = tk.Button(text="Remove fragments", width=toggleWidth, relief="sunken",background="lightgreen", command=toggleRemoveFragments)
else:
    toggleFragments = tk.Button(text="Remove fragments", width=toggleWidth, relief="raised",background="light salmon", command=toggleRemoveFragments)
toggleFragments.grid(row=6,column=boxCol,sticky="e", pady=5)
removeFragments_ttp = CreateToolTip(toggleFragments, "Option to remove proteins with \"(Fragments)\" in the header.")

# Update database
if(cfg['purple']['update_DB']):
    toggleUpdateDB = tk.Button(text="Update database", width=toggleWidth, relief="sunken",background="lightgreen", command=toggleUpdateDB)
else:
    toggleUpdateDB = tk.Button(text="Update database", width=toggleWidth, relief="raised",background="light salmon", command=toggleUpdateDB)
toggleUpdateDB.grid(row=7,column=boxCol,sticky="w", pady=5)
updateDB_ttp = CreateToolTip(toggleUpdateDB, "Build a new database or use the old one.")

# Check targets
if(cfg['purple']['i_am_not_sure_about_target']):
    toggleCheckTargets = tk.Button(text="Check targets", width=toggleWidth, relief="sunken",background="lightgreen", command=toggleCheckTargets)
else:
    toggleCheckTargets = tk.Button(text="Check targets", width=toggleWidth, relief="raised",background="light salmon", command=toggleCheckTargets)
toggleCheckTargets.grid(row=7,column=boxCol,sticky="e", pady=5)
checkTargets_ttp = CreateToolTip(toggleCheckTargets, "Option to check targets before matching peptides.")

# Leucine/Isoleucine distinction
if(cfg['purple']['leucine_distincion']):
    toggleDistinction = tk.Button(text="Isoleucine distinction", width=toggleWidth, relief="sunken",background="lightgreen", command=toggleDistinction)
else:
    toggleDistinction = tk.Button(text="Isoleucine distinction", width=toggleWidth, relief="raised",background="light salmon", command=toggleDistinction)
toggleDistinction.grid(row=9,column=boxCol,sticky="e", pady=5)
distinction_ttp = CreateToolTip(toggleDistinction, "Enable or disable the distinction of leucine and isoleucine")

# Proline digestion
if(cfg['purple']['proline_digestion']):
    toggleProline = tk.Button(text="Proline digestion rule", width=toggleWidth, relief="sunken",background="lightgreen", command=toggleProline)
else:
    toggleProline = tk.Button(text="Proline digestion rule", width=toggleWidth, relief="raised",background="light salmon", command=toggleProline)
toggleProline.grid(row=9,column=boxCol,sticky="w", pady=5)
proline_ttp = CreateToolTip(toggleProline, "Allow digestion if a proline follows on a arginine or lysine.")

# Path to output
outputPath = tk.StringVar()
outputPath.set(cfg['purple']['path_output'])
tk.Label(root, text="Path to output:").grid(row=13,column=namesCol, padx=0, pady=0, sticky='w')
setOutputButton = ttk.Button(root,textvariable = browseText,command=selectOutput)
setOutputButton.grid(row=13,column=boxCol+1, padx=10, pady=0)
setOutputBox = ttk.Entry(root,width=50, textvariable = outputPath, validate ='focus',validatecommand=setOutput)
setOutputBox.grid(row=13,column=boxCol, padx=0, pady=0, sticky='e')
setOutput_ttp = CreateToolTip(setOutputButton, "Select output folder for the Purple results.")


root.mainloop()